
This project contains a small python script that parses the web page
of a given Amazon item and prints seller information. Here is a sample
run::

    $ python scraper.py
    
    ####### Item B00AEBB8OC ########
    
    Main Seller:
        Amazon.com: $5.97
    
    No other sellers
    
    ####### Item B00AMEZDK6 ########
    
    Main Seller:
        Amazon.com: $5.97
    
    Other Sellers:
        Shop Zoombie: $9.89
        msocta: $10.98
        dvdstuff: $12.99

Note that the web pages for the sample items have been saved from the
browser's "View Source". Also, the script depends on the library
"Beautifulsoup" whose details can be found here:
    http://www.crummy.com/software/BeautifulSoup/bs4/doc/
    

