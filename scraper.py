#!/usr/bin/env python

from bs4 import BeautifulSoup

"""

A prototype script that can find main and other seller information
for a given amazon item ID. 

The script uses BeautifulSoup to parse the HTML. 

"""

def get_sellers_info(html_doc):
    """ Parse the item page for seller information and print details. """

    soup = BeautifulSoup(html_doc)
    main_seller = soup.find(id="merchant-info").string.strip()[8:-1]
    main_price = soup.find(id="priceblock_ourprice").string.strip()
    print "\nMain Seller:"
    print "    %s: %s" % (main_seller, main_price)

    other_sellers_info = soup.find_all(class_="mbcMerchantName")
    if other_sellers_info:
        print "\nOther Sellers:"
    else:
        print "\nNo other sellers"

    for other_seller_info in other_sellers_info:
        seller = other_seller_info.string.strip()
        price = other_seller_info.parent.parent.find('div').find('span').string.strip()
        print "    %s: %s" % (seller, price)

    print

def main():
    print
    for sample_item in ["B00AEBB8OC", "B00AMEZDK6"]:
        print "####### Item %s ########" % sample_item
        html_file = "%s.html" % sample_item
        html_doc = open(html_file).read()
        get_sellers_info(html_doc)

if __name__ == '__main__':
    main()
